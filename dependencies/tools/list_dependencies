#!/usr/bin/env python3
#
# Tool to read KDE project metadata to determine which modules are required
# (or recommended) to build a given KDE repository.
#
# See also: http://community.kde.org/Infrastructure/Project_Metadata
#
# Copyright (c) 2014 Michael Pyne <mpyne@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import argparse
import re
import sys
from dataclasses import dataclass
from typing import Dict, KeysView, List, Set, Tuple

version = "0.1"

@dataclass
class Node:
    node: str
    branch: str
    children: List["Node"]

# Handles all aspects of dependency parsing and ordering.
# This is probably too monolithic though -- feel free to fix!
#
# Several conceps are tracked:
# "Direct dependencies": A depends directly on B, like
#   kde/kdemultimedia/juk: kde/kdelibs
#
# "Wildcard dependencies": A grouping of items depend on a specific item, like
#   kde/kdegames/*: frameworks/kcoreaddons
#
# As a special case, "implicit dependencies" are tracked. *All items* depends
#   on these:
#   *: frameworks/kf5umbrella
#
# "Negative dependencies": A dependency that would otherwise be in effect is
#   manually removed. Usually to fixup a wildcard dep, or to account for different
#   possible branches before branch groups were popular. E.g.
#   kde/kdelibs/kactivities: -Qt4[stable]
#   kde/kdelibs/kactivities: Qt5[stable]
#
#   would remove a wildcard dep on Qt4[stable] and add one for Qt5 instead.
#
# Each dependency is really an item-branch pair, where each item is the full
# kde-projects virtual path from the projects database (e.g.
# extragear/utils/kdesrc-build would be an item, but not kdesrc-build). If no
# branch is given (which should be the goal nowadays with branch groups!) then
# '*' is used.
#
# The dependency-data file that is read is dependent upon the branch group, and
# can change dependencies accordingly, even for projects that participate in
# multiple such groups.
#
# The data structure in use for dependencies is basically (for foo[foo-branch]:
# bar[bar-branch]):
#
# dependencies = {
#   foo: {
#     foo-branch: {
#       bar: bar-branch,
#       # and other deps for foo[foo-branch]
#       # it is now not legal for other dependencies of foo[foo-branch] on bar
#       # besides '*' or bar-branch
#     },
#     # other deps for foo[some-other-branch]
#   }
# }
class KDEDependencies:
    def __init__(self, fileName: str) -> None:
        self.memoizedDependencies: Dict[str, Node] = {}
        self.wildcardDependencies: Dict[str, str] = {}
        self.dependencies, self.negativeDeps, self.wildcardItems \
                = self.importDependencies(fileName)
        self.implicitDependencies: Dict[str, str] = self.dependencies.get('*', {}).get('*', {})
        self.showDirectOnly = False

    def setShowDirectOnly(self, showDirectOnly: bool) -> None:
        """Whether or not to show all dependencies recursively, or only direct
        dependencies. Note: I wasn't able to implement correct ordering if
        direct dependencies only are shown. It turns out to be quite nuanced too
        so be careful if you fix this yourself, as getting the ordering right
        still requires going recursively through every possible dependency.
        """
        self.showDirectOnly = showDirectOnly

    def importDependencies(self, fileName: str) -> Tuple[
        Dict[str, Dict[str, Dict[str, str]]],
        Dict[str, Dict[str, Dict[str, str]]],
        Set[str],
    ]:
        """Read dependencies in from the given file."""
        dependencies: Dict[str, Dict[str, Dict[str, str]]] = {}
        negativeDeps: Dict[str, Dict[str, Dict[str, str]]] = {}
        wildcardItems: Set[str] = set()

        with open(fileName, 'r', encoding="utf-8") as file:
            for line in file:
                line = line.partition('#')[0].lstrip()
                if not line:
                    continue
                lineParts = line.partition(':')
                repoItem = lineParts[0].lstrip().rstrip()
                dependentItem = lineParts[2].lstrip().rstrip()

                repo, repoBranch = self.itemToPathAndBranch(repoItem)
                if repo.endswith('/*'):
                    wildcardItems.add(repo.rstrip('/*'))

                negativeDep = False
                if dependentItem.startswith('-'):
                    negativeDep = True
                    dependentItem = dependentItem.lstrip('-')
                dependency, dependencyBranch = self.itemToPathAndBranch(dependentItem)

                dictRepoItem = None
                if negativeDep:
                    dictRepoItem = negativeDeps.setdefault(repo, {})
                else:
                    dictRepoItem = dependencies.setdefault(repo, {})

                dictBranchItem = dictRepoItem.setdefault(repoBranch, {})
                if dependency in dictBranchItem:
                    # Verify same branch
                    curBranchDep = dictBranchItem[dependency]
                    if curBranchDep != dependencyBranch:
                        msg = f'{repo}:{repoBranch} depends on {dependency} and two of its branches, {dependencyBranch} and {curBranchDep}'
                        raise RuntimeError(msg)

                dictBranchItem[dependency] = dependencyBranch

        return dependencies, negativeDeps, wildcardItems

    def itemToPathAndBranch(self, item: str) -> Tuple[str, str]:
        """Splits a foo[foo-branch] dependency into its item and branch pairs"""

        # Look for match everything up to [, then [,
        # then match to end of line and find the last ]
        result = re.search(r'(^[^\[]*)[\[](.*)]$', item)
        if result:
            return result.group(1), result.group(2)
        return item, '*' # No branch, use wildcard

    def keyFromModuleBranch(self, module: str, branch: str) -> Tuple[str, str]:
        """Merges module and branch into a single item for storage"""
        return (module, branch)

    #
    # For the following functions, we keep track of a concept of "dependency
    # candidates" due to the negative dependencies and wildcards. Basically we
    # add all possible implicit and wildcard dependencies for a module (using
    # dependencies that don't care about the branch, and dependencies that care
    # about the branch we're using), and then remove negative dependencies
    # (again, ones that are branch-independent and ones that match branches
    # present in the list).
    #

    def _addModuleBranchDirectDependencies(self, depCandidates: List[Tuple[str, str]], module: str, branch: str) -> None:
        if module not in self.dependencies or branch not in self.dependencies[module]:
            return
        for depModule, depBranch in self.dependencies[module][branch].items():
            if depModule == module:
                continue
            newKey = self.keyFromModuleBranch(depModule, depBranch)
            if newKey not in depCandidates:
                depCandidates.append(newKey)

    def _removeModuleBranchNegativeDependencies(
        self, depCandidates: List[Tuple[str, str]], module: str, branch: str
    ) -> None:
        if module not in self.negativeDeps or branch not in self.negativeDeps[module]:
            return
        for depModule, depBranch in self.negativeDeps[module][branch].items():
            if depModule == '*':
                # The [:] is just to ensure we're assigning to the list passed
                # in to make depCandidates a mutable parameter, otherwise it
                # would only be a local effect.
                depCandidates[:] = [x for x in depCandidates if not x[0] == depModule]
            else:
                key = self.keyFromModuleBranch(depModule, depBranch)
                depCandidates[:] = [x for x in depCandidates if x != key]

    # Adds all effective dependencies of the given module/branch, storing
    # the result as a tree under node. To be useful the tree of node and its
    # children must still be processed to get the list of dependencies.
    def _addEffectiveDeps(self, node: Node, module: str, branch: str) -> None:
        depCandidates: List[Tuple[str, str]] = []
        for w in self.wildcardItems:
            if not module.endswith('/*') and module.startswith(w + '/'):
                wildcardRepo = w + "/*"
                self._addModuleBranchDirectDependencies(depCandidates, wildcardRepo, '*')

        if module not in self.implicitDependencies:
            for depModule, depBranch in self.implicitDependencies.items():
                depCandidates.append(self.keyFromModuleBranch(depModule, depBranch))

        self._addModuleBranchDirectDependencies(depCandidates, module, '*')
        self._addModuleBranchDirectDependencies(depCandidates, module, branch)
        self._removeModuleBranchNegativeDependencies(depCandidates, module, '*')
        self._removeModuleBranchNegativeDependencies(depCandidates, module, branch)

        # Don't let modules depend on themselves by accident
        key = self.keyFromModuleBranch(module, branch)
        depCandidates = list(filter(lambda x: x != key, depCandidates))

        for candidate in depCandidates:
            depModule, depBranch = candidate
            newNode = self._findDepsInternal(depModule, depBranch)
            node.children.append(newNode)

    # Finds all dependencies recursively for the given module and branch,
    # returns a "node" structure (which is itself a tree) describing the
    # dependencies and their proper order.
    def _findDepsInternal(self, module: str, branch: str) -> Node:
        node = self.memoizedDependencies.get(module, None)
        if not node:
            node = Node(module, branch, [])
            if module not in self.implicitDependencies:
                self._addEffectiveDeps(node, module, branch)
            self.memoizedDependencies[module] = node
        else:
            if node.branch != branch and branch != "*" and node.branch != "*":
                raise RuntimeError(f"{module} depends on branch {branch} and on branch {node.branch}!")

        return node

    def printTree(self, node: Node, level: int=0) -> None:
        """Takes the "node" as returned from _findDepsInternal and pretty prints
        a tree version of the dependencies, without removing common deps.
        """
        branch = node.branch
        spacing = ' '.ljust(level)
        if branch != '*':
            print(f"{spacing}{node.node}[{branch}]")
        else:
            print(f"{spacing}{node.node}")

        for child in node.children:
            self.printTree(child, level + 2)

    def printableModuleBranch(self, module: str, branch: str) -> str:
        """Prints a module/branch combination, showing the branch only if it was
        actually set or otherwise mentioned (most dependencies are
        branch-independent).
        """
        if branch != '*':
            return f"{module}[{branch}]"
        return module

    def printOrdering(self, node: Node, visitedSet: Set[str]) -> None:
        """Takes a "node" returned by _findDepsInternal and prints all of the
        dependencies in pre-order fashion. Dependency items are only printed
        once, the first time they are encountered.
        """
        module = node.node
        branch = node.branch

        if module not in visitedSet:
            visitedSet.add(module)
            for child in node.children:
                self.printOrdering(child, visitedSet)
            print(self.printableModuleBranch(module, branch))

    def findDependenciesOf(self, modules: List[str], branch: str) -> None:
        """Finds dependencies of the given modules (plural) and prints them."""
        if self.showDirectOnly:
            # TODO: Fix to keep right order. Set setShowDirectOnly's comment
            for module in modules:
                print(f"{module}:")
                node = self._findDepsInternal(module, branch)
                for child in node.children:
                    module, branch = child.node, child.branch
                    print("\t", self.printableModuleBranch(module, branch))
        else:
            # Fake supporting multiple module paths by merging into virtual dependent
            rootModule = "*"
            self.dependencies[rootModule] = { "*": { x : branch for x in modules } }

            node = self._findDepsInternal(rootModule, branch)
            visitSet: Set[str] = set()
            for child in node.children:
                self.printOrdering(child, visitSet)

    def contains(self, module: str) -> bool:
        """Does the module occur in the dependencies?"""
        return module in self.dependencies

    def allModules(self) -> KeysView[str]:
        """Return an iterator over all module names."""
        return self.dependencies.keys()

def addPathIfMissing(
    deps: KDEDependencies, modules: List[str], ignore_missing: bool = False
) -> Tuple[List[str], List[str]]:
    """Partition a list of modules into good and bad modules

    The good modules will be expanded to the full path (if they are not already
    the full path) by finding the first module in the list of dependencies where
    the name matches that of the dependency.
    """
    good = []
    bad = []
    for m in modules:
        if deps.contains(m):
            good.append(m)
        elif "/" not in m:
            found = False
            for mod in deps.allModules():
                if mod.endswith("/"+m):
                    good.append(mod)
                    found = True
                    break
            if not found:
                bad.append(m)
        elif ignore_missing:
            # Some modules may be logically present without any directly-listed
            # dependencies, so we can't always assume this is an error.
            good.append(m)
        else:
            bad.append(m)

    return (good, bad)

def main() -> None:
    """The main function of this script"""
    arg_parser = argparse.ArgumentParser(
            description="Shows the git.kde.org dependencies of git.kde.org modules.")
    arg_parser.add_argument("-d", "--direct-dependencies", action="store_true",
            help="Shows *unordered* direct dependencies only (default is recursive).")
    arg_parser.add_argument("-g", "--branch-group", default="kf5-qt5",
            help="Branch group to use for dependencies (stable-qt4, latest-qt4, or kf5-qt5)")
    arg_parser.add_argument("-b", "--branch", default="*",
            help="Specific repository branch to find dependencies of (prefer --branch-group though). Default is '*'")
    arg_parser.add_argument("module_path",
            nargs="+",
            help=
        """KDE project module path e.g. kde/kdelibs. If multiple paths are
        specified (with the -d option), they have their branches printed
        one-per-line in the order listed. Without the -d option, dependencies
        are shown recursively, and in the needed build order, *including* the
        module paths passed in on the command line.
        """
    )
    arg_parser.add_argument("-m", "--metadata-path",
            default="../",
            help="Path to kde-build-metadata *directory*")
    arg_parser.add_argument("-f", "--assume-present", action='store_true',
            help="If set, assume all input modules are present, and list implicit dependencies")
    arg_parser.add_argument("-v", "--version",
            action='version', version='%(prog)s ' + str(version))
    args = arg_parser.parse_args()

    deps = KDEDependencies(f"{args.metadata_path}/dependency-data-{args.branch_group}")
    deps.setShowDirectOnly(args.direct_dependencies)

    (modules, mistake_modules) = addPathIfMissing(deps, args.module_path, args.assume_present)
    if len(mistake_modules) > 0:
        print("Error: Couldn't find the following modules:")
        for module in mistake_modules:
            print(f"\t{module}")
        sys.exit(1)
    else:
        deps.findDependenciesOf(modules, args.branch)

if __name__ == '__main__':
    main()
